iTexMo-API Client (iTexMo Player)
=======================

A Python/C Programming language and pure OpenGL (Graphics) based Application that works on iOS,Android,Linux,Mac and Windows, and wait It's Source is Open!

------


**Useful for** Using iTexMo-API (Python) Functionalities, Such as Sending Messages, Requests and Tweaking API itself.

**Also** You can use different API's (such as Twillio, etc.) within the App.


Screenshot:

![preview](Data/images/shot.png)