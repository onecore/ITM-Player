__information__ = """
Author: Mark Anthony Pequeras
Website: http://marp.me
Source: https://gitlab.com/onecore/ITM-Player
Information: iTexMo-API Mobile player (API must be provide to use this Application)
Platform: Windows, Mac, Linux, Android and iOS
Requirements: Python, Kivy and OpenGL >= 2
"""

__Builder__ = ("Python","Cython", "C")
__Platform__ = ("Android","iOS","Linux","Mac","Windows")
__Debug__ = False
__OtherAPI__ = { False : None }
__Console__ = False

from iTexMoAPI.itexmo import Imapi      # iTexMo-API (Python Version)
from kivy.app import App                # Executor
from kivy.lang import Builder           # Kivy Language Parser



class ItexException(Exception):
    """
    This Class activates only in Desktop Platform.
    """

    pass


class iTexMoAPI(Imapi):
    """
    Class that Overrides Imapi() function from iTexMO-API
    """

    pass


class ItexMoPlayerEngine(object):
    """
    A Group of App functions below.
    This Class contains all of the functions for the App process.
    """


    @staticmethod
    def setGeometry(X,Y):
        """
        Dict Shortener
        """
        _Geometry = lambda X,Y: {"center_x" : X, "center_y" : Y} if X and Y else False
        return _Geometry(X,Y)


    @staticmethod
    def ShortFloat(object):
        """
        Short Floating Point type Check.
        """
        _ShortFloat = lambda object: True if isinstance(object,float) else False
        return _ShortFloat(object)

    @staticmethod
    def Geometry(X,Y):
        """
        Function as short-cut for Positioning.
        """
        _Geometry = lambda X,Y: {"center_x" : X, "center_y" : Y} if X and Y else False
        return _Geometry(X,Y)

    @staticmethod
    def CenterGeometry(bool):
        """
        Function for Passing X,Y Coordinates (.5,.5=50%,50%)
        """
        _CenterGeometry = lambda Access: {"center_x" : .5, "center_y" : .5} if Access else False
        return _CenterGeometry(bool)

    @staticmethod
    def Color(string):
        """
        for Parsing HTML Colors
        """
        from kivy.parser import parse_color

        _Color = lambda p: parse_color(p)
        String = string

        if isinstance(String,str):
            Hash = String[0]
            Parse = None

            if "#" == Hash:
                Parse = String
            else:
                Parse = "#"+str(String)

            return _Color(Parse)

        else:
            raise ItexException("color string Needed, e.g #ffffff")


class ItexMoPlayer(App):
    """
    Main Class and holds widget.
    """

    def ___init__(self):
        self.Screenies = []

    def build(self):
        """
        Overrides build Method
        """

        self.title = "iTexMo API Player"
        return self.ScreenEngineObj()

    def ScreenSwitcher(self,screen):
        """
        Function for Switching Screens (defaults to 'Main')
        """
        pass

    def ScreenEngineObj(self):
        """
        Main Screen Manager (Holds other Screen objects)
        """
        from kivy.uix.screenmanager import ScreenManager

        self.ScreenManagerMain = ScreenManager()
        self.ScreenManagerMain.add_widget(self.MainScreenObj())     # Main
        self.ScreenManagerMain.add_widget(self.iTexScreenObj())     # iTex
        #self.ScreenManagerMain.add_widget(self.OptionsViewObj())    # Options
        self.ScreenManagerMain.current = "Main"

        self.Screenies = list(self.ScreenManagerMain.screen_names)  # Store listed Screens (needed as its Global)

        return self.ScreenManagerMain


    def MainScreenObj(self):
        """
        Function that contains objects of Introduction of the App.
        """

        from kivy.uix.screenmanager import Screen
        from kivy.uix.button import Button
        from kivy.uix.floatlayout import FloatLayout
        Api = iTexMoAPI()

        def Move(int):
            """
            Move to a given Screen (int)
            """
            self.ScreenManagerMain.current = self.Screenies[int]

        GoItex = Button()
        GoItex.pos_hint = ItexMoPlayerEngine.CenterGeometry(True)
        #GoItex.on_press = lambda: Move(1)                            # Simple hack rather than functools. :)
        GoItex.background_normal = "Data/images/bg.png"
        GoItex.background_down = GoItex.background_normal

        Logo = Button()
        Logo.background_normal = "Data/images/logo.jpg"
        Logo.background_down = Logo.background_normal
        Logo.pos_hint = ItexMoPlayerEngine.setGeometry(.5,.7)
        Logo.size_hint = 1,.24

        EnterMain = Button()
        EnterMain.background_normal = "Data/images/send.jpg"
        EnterMain.background_down = EnterMain.background_normal
        EnterMain.size_hint = .9,.12
        EnterMain.pos_hint = ItexMoPlayerEngine.setGeometry(.3,.36)

        EnterOptions = Button()
        EnterOptions.background_normal = "Data/images/OPTIONS.jpg"
        EnterOptions.background_down = EnterOptions.background_normal
        EnterOptions.on_release = lambda: self.OptionsViewObj()
        EnterOptions.pos_hint = ItexMoPlayerEngine.setGeometry(.5,.3)
        EnterOptions.size_hint = 1,.27

        ApiInfo = Button()
        ApiInfo.background_normal = "Data/images/api.jpg"
        ApiInfo.background_down = ApiInfo.background_normal
        ApiInfo.pos_hint = ItexMoPlayerEngine.setGeometry(.87,.36)
        ApiInfo.size_hint = .29,.122

        BubblePlayer = Button()
        BubblePlayer.background_normal = "Data/images/player.jpg"
        BubblePlayer.background_down = BubblePlayer.background_normal
        BubblePlayer.pos_hint = ItexMoPlayerEngine.setGeometry(.75,.61)
        BubblePlayer.size_hint = .4,.1

        MainLayout = FloatLayout()
        MainLayout.add_widget(GoItex)
        MainLayout.add_widget(Logo)
        MainLayout.add_widget(EnterOptions)
        MainLayout.add_widget(ApiInfo)
        MainLayout.add_widget(EnterMain)
        MainLayout.add_widget(BubblePlayer)

        MainObj = Screen(name="Main")
        MainObj.add_widget(MainLayout)

        return MainObj


    def iTexScreenObj(self):
        """
        Function that contains objects of Main Messaging Area
        """
        from kivy.uix.screenmanager import Screen

        MainObj = Screen(name="iTex")
        return MainObj


    def OptionsViewObj(self):
        """
        Function that contains objects of Options Area
        """
        from kivy.uix.modalview import ModalView
        from kivy.uix.floatlayout import FloatLayout
        from kivy.uix.tabbedpanel import TabbedPanel, TabbedPanelHeader
        from kivy.uix.button import Button


        # 3 Tabbed Options {
        _a = "Data/images/yellow.jpg"
        _d = "Data/images/ydown.jpg"

        OptionMessaging = TabbedPanelHeader()   # Messaging Options
        OptionMessaging.background_normal = _a
        OptionMessaging.background_down = _d
        OptionMessaging.text = "Main"

        OptionAPI = TabbedPanelHeader()         # API Tweaks
        OptionAPI.background_normal = _a
        OptionAPI.background_down = _d
        OptionAPI.text = "API"

        OptionDocs = TabbedPanelHeader()        # Docs View
        OptionDocs.background_normal = _a
        OptionDocs.background_down = _d
        OptionDocs.text = "Docs"
        # 3 Tabbed Options  }

        class MLContents(object):
            """
            Inner Class with Message Layout Contents
            - Options for Main
            """
            from kivy.uix.button import Button
            from kivy.uix.scrollview import ScrollView

            Builder.load_strings = """
<MessageView>:

    Label:
        id: PCHATFEEDENGINE
        size_hint_y: None
        height: self.texture_size[1]
        text_size: self.width, None
        text: root.text
"""

            @classmethod
            class MessageView(ScrollView):
                """
                Overrides ScrollView
                - This is a Widget and not a Function.
                """
                from kivy.properties import StringProperty
                text = StringProperty('')


            @staticmethod
            def Main():
                Layout = FloatLayout()
                x = Button(text="aaaa")

                Layout.add_widget(x)
                return Layout


        class APILContents(object):
            """
            Inner Class with API Contents
            - Options for API
            """
            @staticmethod
            def Main():
                Layout = FloatLayout()
                x = Button(text="bbb")

                Layout.add_widget(x)
                return Layout


        class DocsLContents(object):
            """
            Inner Class with Docs Contents
            - Options for Docs
            """
            @staticmethod
            def Main():
                Layout = FloatLayout()
                x = Button(text="c")

                Layout.add_widget(x)
                return Layout


        _MessageLayout = MLContents.Main()     # Default
        _ApiLayout = APILContents.Main()       # 2nd
        _DocsLayout = DocsLContents.Main()     # 3rd

        OptionMessaging.content = _MessageLayout
        OptionAPI.content = _ApiLayout
        OptionDocs.content = _DocsLayout

        OptionTabberObj = TabbedPanel()
        OptionTabberObj.default_tab_content = MLContents.Main()
        OptionTabberObj.do_default_tab = False # True will be so ugly.
        OptionTabberObj.pos_hint = ItexMoPlayerEngine.setGeometry(.5,.5)
        OptionTabberObj.add_widget(OptionMessaging)
        OptionTabberObj.add_widget(OptionAPI)
        OptionTabberObj.add_widget(OptionDocs)

        OptionsLayout = FloatLayout()
        OptionsLayout.add_widget(OptionTabberObj)

        OptionsObj = ModalView(auto_dismiss=True)
        OptionsObj.add_widget(OptionsLayout)
        OptionsObj.size_hint = .85,.85
        OptionsObj.open()


# If you want to make this Source importable, Remove this Condition
if __name__ == "__main__":
    iTexPlayer = ItexMoPlayer()
    iTexPlayer.run()




